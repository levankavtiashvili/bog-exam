﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BOG.Helper;

namespace BOG.DAL.Implementation
{
    public class BaseDal
    {
        #region Private  Fields

        private readonly string _connectionString;

        #endregion

        #region Constructors

        public BaseDal()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #endregion

        #region Methods

        protected CommandConnectionWrapper GetSpCommand(string commandText)
        {
            var sqlConnection = GetConnection();
            var sqlcommand = sqlConnection.CreateCommand();
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.CommandText = commandText;
            return new CommandConnectionWrapper(sqlcommand, sqlConnection);
        }

        protected void AddParameter(SqlCommand command, string parameterName, object parameterValue)
        {
            command.Parameters.Add(new SqlParameter { ParameterName = parameterName, Value = parameterValue });
        }

        private SqlConnection GetConnection()
        {
            var dbConnection = new SqlConnection(_connectionString);
            dbConnection.Open();
            return dbConnection;
        }
        #endregion
    }
}