﻿using System.Data.Entity;

namespace BOG.DAL.Implementation
{
    public class Bog : DbContext
    { 
        public Bog() : base("name=DefaultConnection")
        {
            Database.SetInitializer<Bog>(null);
        }

        public DbSet<Models.Product> Products { get; set; }

        public DbSet<Models.Consultant> Consultants { get; set; }

        public DbSet<Models.Sale> Sales { get; set; }

        public DbSet<Models.SaleDetail> SaleDetails { get; set; }
    }
}
