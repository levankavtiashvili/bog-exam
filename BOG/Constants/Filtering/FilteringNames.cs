﻿namespace BOG.Constants.Filtering
{
    public class FilteringNames
    {
        public const string IdDesc = "Id_desc";
        public const string SaleId = "saleId";
        public const string SaleIdDesc = "saleId_desc";
        public const string ProductId = "productId";
        public const string ProductIdDesc = "productId_desc";
        public const string Amount = "amount";
        public const string AmountDesc = "amount_desc";
        public const string HierarchyAmount = "hiearchyAmount";
        public const string HierarchyAmountDesc = "hiearchyAmount_desc";
        public const string ConsultantId = "consultantId";
        public const string ConsultantIdDesc = "consultantId_desc";
        public const string Name = "name";
        public const string NameDesc = "name_desc";
        public const string LastName = "lastName";
        public const string LastNameDesc = "lastName_desc";
        public const string BirthDay = "birthDate";
        public const string BirthDayDesc = "birthDate_desc";
        public const string SaleDate = "saleDate";
        public const string SaleDateDesc = "saleDate_desc";
        public const string PersonalId = "personalId";
        public const string PersonalIdDesc = "personalId_desc";
        public const string Gender = "gender";
        public const string GenderDesc = "gender_desc";
        public const string RecomendatorId = "recomendatorId";
        public const string RecomendatorIdDesc = "recomendatorId_desc";
        public const string Price = "price";
        public const string PriceDesc = "price_desc";
        public const string CreatedAt = "createdAt";
        public const string CreatedAtDesc = "createdAt_desc";
        public const string MaxSaledProductId = "maxSaledProductId";
        public const string MaxSaledProductIdDesc = "maxSaledProductId_desc";
        public const string MaxSaledProductName = "maxSaledProductName";
        public const string MaxSaledProductNameDesc = "maxSaledProductName_desc";
        public const string MaxSaledProductAmount = "maxSaledProductAmount";
        public const string MaxSaledProductAmountDesc = "maxSaledProductAmount_desc";
        public const string MaxProfitProductId = "maxProfitProductId";
        public const string MaxProfitProductIdDesc = "maxProfitProductId_desc";
        public const string MaxProfitProductName = "maxProfitProductName";
        public const string MaxProfitProductNameDesc = "maxProfitProductName_desc";
        public const string MaxProfitSum = "maxProfitSum";
        public const string MaxProfitSumDesc = "maxProfitSum_desc";
        public const string TotalPrice = "totalPrice";
        public const string TotalPriceDesc = "totalPrice_desc";
    }
}