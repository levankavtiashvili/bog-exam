﻿using System;
using System.Configuration;

namespace BOG.Constants.Paging
{
    public class Paging
    {
        public static readonly int PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
    }
}