﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BOG.Models
{
    public class SaleDetail
    {
        // Primary Key
        public long Id { get; set; }

        public long SaleId { get; set; }

        public long ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        public int Amount { get; set; }
    }
}