﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BOG.Models
{
    public class Consultant
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        [Index("Consultants_Personal_Id_Unique", 1, IsUnique = true)]
        public long PersonalId { get; set; }

        public Gender Gender { get; set; }

        public DateTime BirthDate { get; set; }

        public int? RecomendatorId { get; set; }

        public List<Sale> Sales { get; set; }
    }
}