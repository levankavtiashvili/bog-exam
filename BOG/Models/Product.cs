﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOG.Models
{
    public class Product
    {
        // Primary Key
        public long Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }
    }
}