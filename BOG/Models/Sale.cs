﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BOG.Models
{
    public class Sale
    {
        // Primary Key
        public long Id { get; set; }

        public int ConsultantId { get; set; }

        [ForeignKey("ConsultantId")]
        public virtual Consultant Consultant { get; set; }
        
        public DateTime CreatedAt { get; set; }

        public List<SaleDetail> SaleDetails { get; set; }
    }
}