﻿namespace BOG.Models.SearchModels
{
    public class FrequentlySaledProduct : SearchConsultantModel
    {
        public int ProductAmount { get; set; }

        public long ProductId { get; set; }
    }
}