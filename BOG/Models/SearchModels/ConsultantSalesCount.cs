﻿using System;

namespace BOG.Models.SearchModels
{
    public class ConsultantSalesCount : SearchConsultantModel
    {
        public int? RecomendatorId { get; set; }

        public int SalesAmount { get; set; }

        public int HierarchySalesAmount { get; set; }
    }
}