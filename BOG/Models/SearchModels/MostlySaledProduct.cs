﻿using System;

namespace BOG.Models.SearchModels
{
    public class MostlySaledProduct : SearchConsultantModel
    {
        public long MaxSaledProductId { get; set; }

        public string MaxSaledProductName { get; set; }

        public int MaxSaledProductAmount { get; set; }

        public long MaxProfitProductId { get; set; }

        public string MaxProfitProductName { get; set; }

        public decimal MaxProfitTotalSum { get; set; }
    }
}