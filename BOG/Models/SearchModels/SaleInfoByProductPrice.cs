﻿using System;

namespace BOG.Models.SearchModels
{
    public class SaleInfoByProductPrice : SearchConsultantModel
    {
        public long SaleId { get; set; }

        public int ProductAmount { get; set; }

        public DateTime SalesDate { get; set; }
    }
}