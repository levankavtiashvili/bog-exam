﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOG.Models.SearchModels
{
    public class ListConsultantSales
    {
        public List<ConsultantSalesCount> Count { get; set; }
    }
}