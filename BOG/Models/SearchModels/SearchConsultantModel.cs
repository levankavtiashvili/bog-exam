﻿using System;

namespace BOG.Models.SearchModels
{
    public class SearchConsultantModel
    {
        public int ConsultantId { get; set; }

        public string ConsultantName { get; set; }

        public string ConsultantLastName { get; set; }

        public long ConsultantPersonalId { get; set; }

        public DateTime ConsultantBirthDate { get; set; }
    }
}