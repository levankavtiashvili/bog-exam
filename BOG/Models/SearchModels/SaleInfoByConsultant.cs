﻿using System;

namespace BOG.Models.SearchModels
{
    public class SaleInfoByConsultant: SearchConsultantModel
    {
        public long SaleId { get; set; }

        public int ProductAmount { get; set; }

        public decimal TotalPrice { get; set; }

        public DateTime SalesDate { get; set; }
    }
}