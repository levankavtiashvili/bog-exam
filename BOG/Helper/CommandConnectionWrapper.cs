﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace BOG.Helper
{
    public class CommandConnectionWrapper : IDisposable
    {
        private bool _disposed;

        public SqlConnection SqlConnection { get; private set; }

        public SqlCommand SqlCommand { get; private set; }

        public CommandConnectionWrapper(SqlCommand sqlCommand, SqlConnection sqlConnection = null)
        {
            SqlCommand = sqlCommand;
            SqlConnection = sqlConnection;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~CommandConnectionWrapper()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                if (SqlCommand != null)
                {
                    SqlCommand.Dispose();
                    SqlCommand = null;
                }
                if (SqlConnection != null)
                {
                    CloseConnection();
                    SqlConnection.Dispose();
                    SqlConnection = null;
                }
            }
            _disposed = true;
        }

        private void CloseConnection()
        {
            if (SqlConnection.State == ConnectionState.Open)
            {
                SqlConnection.Close();
            }
        }
    }
}