﻿using BOG.DAL.Implementation;
using BOG.DAL.Interface;
using StructureMap;

namespace BOG.Helper
{
    public class TypeRegistrator : Registry
    {
        public TypeRegistrator()
        {
            Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
            
            For<IConsultantDal>().Use<ConsultantDal>();
        }
    }
}