﻿using StructureMap;

namespace BOG.Helper
{
    public static class IoC
    {
        private static volatile IContainer _instance;
        private static readonly object SyncRoot = new object();

        public static IContainer Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }
                lock (SyncRoot)
                {
                    if (_instance == null)
                    {
                        _instance = new Container(c => c.AddRegistry<TypeRegistrator>());
                    }
                }
                return _instance;
            }
        }
    }
}