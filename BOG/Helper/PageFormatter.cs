﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOG.Helper
{
    public class PageFormatter
    {
        public static int GetValidPage(int? page)
        {
            return page ?? 1;
        }
    }
}