﻿using System.Web.Mvc;

namespace BOG.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}