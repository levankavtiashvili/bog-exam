﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using BOG.Constants.Filtering;
using BOG.Constants.Paging;
using BOG.DAL.Implementation;
using BOG.Helper;
using BOG.Models;
using PagedList;

namespace BOG.Controllers
{
    public class ConsultantsController : Controller
    {
        private readonly Bog _db = new Bog();

        // GET: Consultants
        public ActionResult Index(string sortOrder, int? searchId, string searchName, string searchLastName, long? searchPersonalId, int? searchRecomendatorId, DateTime? searchBirthDate, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilterId = searchId;
            ViewBag.CurrentFilterName = searchName;
            ViewBag.CurrentFilterLastName = searchLastName;
            ViewBag.CurrentFilterPersonalId = searchPersonalId;
            ViewBag.CurrentFilterRecomendatorId = searchRecomendatorId;
            ViewBag.CurrentFilterBirthDate = searchBirthDate;
            ViewBag.IdSortParam = string.IsNullOrEmpty(sortOrder) ? FilteringNames.IdDesc : "";
            ViewBag.NameSortParam = sortOrder == FilteringNames.Name ? FilteringNames.NameDesc: FilteringNames.Name;
            ViewBag.LastNameSortParam = sortOrder == FilteringNames.LastName ? FilteringNames.LastNameDesc: FilteringNames.LastName;
            ViewBag.PersonalIdSortParam = sortOrder == FilteringNames.PersonalId ? FilteringNames.PersonalIdDesc : FilteringNames.PersonalId;
            ViewBag.GenderSortParam = sortOrder == FilteringNames.Gender ? FilteringNames.GenderDesc: FilteringNames.Gender;
            ViewBag.BirthDateSortParam = sortOrder == FilteringNames.BirthDay ? FilteringNames.BirthDayDesc : FilteringNames.BirthDay;
            ViewBag.RecomendatorIdSortParam = sortOrder == FilteringNames.RecomendatorId ? FilteringNames.RecomendatorIdDesc : FilteringNames.RecomendatorId;
            var consultants = _db.Consultants.ToList();
            if (searchId.HasValue)
            {
                consultants = consultants.Where(s => s.Id == searchId).ToList();
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                consultants = consultants.Where(s => s.Name.Contains(searchName)).ToList();
            }
            if (!string.IsNullOrEmpty(searchLastName))
            {
                consultants = consultants.Where(s => s.LastName.Contains(searchLastName)).ToList();
            }
            if (searchPersonalId.HasValue)
            {
                consultants = consultants.Where(s => s.PersonalId == searchPersonalId.Value).ToList();
            }
            if (searchRecomendatorId.HasValue)
            {
                consultants = consultants.Where(s => s.RecomendatorId == searchRecomendatorId.Value).ToList();
            }
            if (searchBirthDate != null)
            {
                consultants = consultants.Where(s => s.BirthDate.Day == searchBirthDate.Value.Day
                                                     && s.BirthDate.Month == searchBirthDate.Value.Month
                                                     && s.BirthDate.Year == searchBirthDate.Value.Year).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.IdDesc:
                    consultants = consultants.OrderByDescending(s => s.Id).ToList();
                    break;
                case FilteringNames.LastNameDesc:
                    consultants = consultants.OrderByDescending(s => s.LastName).ToList();
                    break;
                case FilteringNames.NameDesc:
                    consultants = consultants.OrderByDescending(s => s.Name).ToList();
                    break;
                case FilteringNames.BirthDayDesc:
                    consultants = consultants.OrderByDescending(s => s.BirthDate).ToList();
                    break;
                case FilteringNames.GenderDesc:
                    consultants = consultants.OrderByDescending(s => s.Gender).ToList();
                    break;
                case FilteringNames.PersonalIdDesc:
                    consultants = consultants.OrderByDescending(s => s.PersonalId).ToList();
                    break;
                case FilteringNames.RecomendatorIdDesc:
                    consultants = consultants.OrderByDescending(s => s.RecomendatorId).ToList();
                    break;
                case FilteringNames.LastName:
                    consultants = consultants.OrderBy(s => s.LastName).ToList();
                    break;
                case FilteringNames.Name:
                    consultants = consultants.OrderBy(s => s.Name).ToList();
                    break;
                case FilteringNames.BirthDay:
                    consultants = consultants.OrderBy(s => s.BirthDate).ToList();
                    break;
                case FilteringNames.Gender:
                    consultants = consultants.OrderBy(s => s.Gender).ToList();
                    break;
                case FilteringNames.PersonalId:
                  consultants = consultants.OrderBy(s => s.PersonalId).ToList();
                    break;
                case FilteringNames.RecomendatorId:
                    consultants = consultants.OrderBy(s => s.RecomendatorId).ToList();
                    break;
                default:
                    consultants = consultants.OrderBy(s => s.Id).ToList();
                    break;
            }

            var pagedData = consultants.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(consultants.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        // GET: Consultants/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var consultant = await _db.Consultants.FindAsync(id);
            if (consultant == null)
            {
                return HttpNotFound();
            }
            consultant.Sales = _db.Sales.Where(x => x.ConsultantId == consultant.Id).ToList();
            return View(consultant);
        }

        // GET: Consultants/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Consultants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,LastName,PersonalId,Gender,BirthDate,RecomendatorId")] Consultant consultant)
        {
            if (!ModelState.IsValid)
            {
                return View(consultant);
            }
            if (consultant.RecomendatorId.HasValue)
            {
                if (_db.Consultants.FirstOrDefault(x => x.Id == consultant.RecomendatorId.Value) == null)
                {
                    throw new Exception("Recomendator Not Exists");
                }
            }
            _db.Consultants.Add(consultant);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        // GET: Consultants/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var consultant = await _db.Consultants.FindAsync(id);
            if (consultant == null)
            {
                return HttpNotFound();
            }
            return View(consultant);
        }

        // POST: Consultants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,LastName,PersonalId,Gender,BirthDate,RecomendatorId")] Consultant consultant)
        {
            if (!ModelState.IsValid)
            {
                return View(consultant);
            }
            if (consultant.RecomendatorId.HasValue)
            {
                if (consultant.RecomendatorId == consultant.Id)
                {
                    throw new Exception("Consultant is recomending by himself");
                }
                var recomendatorId = consultant.RecomendatorId;
                do
                {
                    var recomendator = await _db.Consultants.FindAsync(recomendatorId);
                    if (recomendator == null)
                    {
                        throw new Exception("Recomendator Does Not Exists");
                    }
                    recomendatorId = recomendator.RecomendatorId;
                    if (recomendatorId == consultant.Id)
                    {
                        throw new Exception("Recomendator Is Circuling");
                    }
                } while (recomendatorId.HasValue);
            }
            _db.Entry(consultant).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Consultants/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var consultant = await _db.Consultants.FindAsync(id);
            if (consultant == null)
            {
                return HttpNotFound();
            }
            return View(consultant);
        }

        // POST: Consultants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            _db.Consultants.Remove(await _db.Consultants.FindAsync(id) ?? throw new InvalidOperationException());
            foreach (var dbConsultant in _db.Consultants)
            {
                if (dbConsultant.RecomendatorId == id)
                {
                    dbConsultant.RecomendatorId = null;
                }
            }
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
