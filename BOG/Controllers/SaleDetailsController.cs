﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using BOG.Constants.Filtering;
using BOG.Constants.Paging;
using BOG.DAL.Implementation;
using BOG.Helper;
using BOG.Models;
using PagedList;

namespace BOG.Controllers
{
    public class SaleDetailsController : Controller
    {
        private readonly Bog _db = new Bog();

        // GET: SaleDetails
        public ActionResult Index(string sortOrder,long? searchId, string searchName, long? searchSaleId, long? searchProductId, int? searchAmount, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilterId = searchId;
            ViewBag.CurrentFilterName = searchName;
            ViewBag.CurrentFilterProductId = searchProductId;
            ViewBag.CurrentFilterSaleId = searchSaleId;
            ViewBag.CurrentFilterAmount = searchAmount;
            ViewBag.IdSortParam = string.IsNullOrEmpty(sortOrder) ? FilteringNames.IdDesc : "";
            ViewBag.NameSortParam = sortOrder == FilteringNames.Name ? FilteringNames.NameDesc : FilteringNames.Name;
            ViewBag.ProductIdSortParam = sortOrder == FilteringNames.ProductId ? FilteringNames.ProductIdDesc : FilteringNames.ProductId;
            ViewBag.SaleIdSortParam = sortOrder == FilteringNames.SaleId ? FilteringNames.SaleIdDesc : FilteringNames.SaleId;
            ViewBag.AmountSortParam = sortOrder == FilteringNames.Amount ? FilteringNames.AmountDesc : FilteringNames.Amount;
            var saleDetails = _db.SaleDetails.ToList();
            if (searchId.HasValue)
            {
                saleDetails = saleDetails.Where(s => s.Id == searchId).ToList();
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                saleDetails = saleDetails.Where(s => s.Product.Name.Contains(searchName)).ToList();
            }
            if (searchProductId.HasValue)
            {
                saleDetails = saleDetails.Where(s => s.ProductId == searchProductId).ToList();
            }
            if (searchSaleId.HasValue)
            {
                saleDetails = saleDetails.Where(s => s.SaleId == searchSaleId).ToList();
            }
            if (searchAmount.HasValue)
            {
                saleDetails = saleDetails.Where(s => s.Amount == searchAmount).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.IdDesc:
                    saleDetails = saleDetails.OrderByDescending(s => s.Id).ToList();
                    break;
                case FilteringNames.SaleIdDesc:
                    saleDetails = saleDetails.OrderByDescending(s => s.SaleId).ToList();
                    break;
                case FilteringNames.ProductIdDesc:
                    saleDetails = saleDetails.OrderByDescending(s => s.ProductId).ToList();
                    break;
                case FilteringNames.NameDesc:
                    saleDetails = saleDetails.OrderByDescending(s => s.Product.Name).ToList();
                    break;
                case FilteringNames.AmountDesc:
                    saleDetails = saleDetails.OrderByDescending(s => s.Amount).ToList();
                    break;
                case FilteringNames.SaleId:
                    saleDetails = saleDetails.OrderBy(s => s.SaleId).ToList();
                    break;
                case FilteringNames.ProductId:
                    saleDetails = saleDetails.OrderBy(s => s.ProductId).ToList();
                    break;
                case FilteringNames.Name:
                    saleDetails = saleDetails.OrderBy(s => s.Product.Name).ToList();
                    break;
                case FilteringNames.Amount:
                    saleDetails = saleDetails.OrderBy(s => s.Amount).ToList();
                    break;
                default:
                    saleDetails = saleDetails.OrderBy(s => s.Id).ToList();
                    break;
            }
            var pagedData = saleDetails.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(saleDetails.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        // GET: SaleDetails/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var saleDetail = await _db.SaleDetails.FindAsync(id);
            if (saleDetail == null)
            {
                return HttpNotFound();
            }
            return View(saleDetail);
        }

        // GET: SaleDetails/Create
        public ActionResult Create()
        {
            ViewBag.SaleIds = _db.Sales.ToListAsync().Result;
            ViewBag.ProductIds = _db.Products.ToListAsync().Result;
            return View();
        }

        // POST: SaleDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,SaleId,ProductId,Amount")] SaleDetail saleDetail)
        {
            if (saleDetail.Amount <= 0)
            {
                throw new Exception("Amount Is Equal Or Less Than Zero");
            }
            if (!ModelState.IsValid)
            {
                return View(saleDetail);
            }
            _db.SaleDetails.Add(saleDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        // GET: SaleDetails/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var saleDetail = await _db.SaleDetails.FindAsync(id);
            if (saleDetail == null)
            {
                return HttpNotFound();
            }
            return View(saleDetail);
        }

        // POST: SaleDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,SaleId,ProductId,Amount")] SaleDetail saleDetail)
        {
            if (saleDetail.Amount <= 0)
            {
                throw new Exception("Amount Is Equal Or Less Than Zero");
            }
            if (!ModelState.IsValid)
            {
                return View(saleDetail);
            }
            _db.Entry(saleDetail).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: SaleDetails/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var saleDetail = await _db.SaleDetails.FindAsync(id);
            if (saleDetail == null)
            {
                return HttpNotFound();
            }
            return View(saleDetail);
        }

        // POST: SaleDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            _db.SaleDetails.Remove(await _db.SaleDetails.FindAsync(id) ?? throw new InvalidOperationException());
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
