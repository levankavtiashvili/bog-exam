﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BOG.Constants.Filtering;
using BOG.Constants.Paging;
using BOG.DAL.Implementation;
using BOG.DAL.Interface;
using BOG.Helper;
using BOG.Models.SearchModels;
using PagedList;

namespace BOG.Controllers
{
    public class SaleDetailSearchController : Controller
    {
        private readonly IConsultantDal _consultantDal = IoC.Instance.GetInstance<ConsultantDal>();

        // GET: SaleInfoByProductPrice
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSaleInfoByProductPrice(string sortOrder, int? searchConsultantId, string searchConsultantName, string searchConsultantLastName, long? searchConsultantPersonalid, long? searchSaleId, int? searchProductAmount, decimal? minPrice, decimal? maxPrice, DateTime? startDate, DateTime? endDate, int? page)
        {
            if (!minPrice.HasValue || !maxPrice.HasValue || !startDate.HasValue || !endDate.HasValue)
            {
                return View(new List<SaleInfoByProductPrice>().ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize));
            }
            ModifySort(sortOrder);
            ModifySearchingParameter(startDate, endDate, minPrice: minPrice, maxPrice : maxPrice);
            ViewBag.CurrentSort = sortOrder;

            ModifyConsultantFiltering(searchConsultantId, searchConsultantName, searchConsultantLastName, searchConsultantPersonalid);
            ViewBag.CurrentFilterProductAmount = searchProductAmount;
            ViewBag.CurrentFilterSaleId = searchSaleId;

            var salesInfoByProductPrices = _consultantDal.GetSaleInfoProductPrice(minPrice.Value, maxPrice.Value, startDate.Value, endDate.Value);
            if (searchConsultantId.HasValue)
            {
                salesInfoByProductPrices = salesInfoByProductPrices.Where(s => s.ConsultantId == searchConsultantId).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantName))
            {
                salesInfoByProductPrices = salesInfoByProductPrices.Where(s => s.ConsultantName.Contains(searchConsultantName)).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantLastName))
            {
                salesInfoByProductPrices = salesInfoByProductPrices.Where(s => s.ConsultantLastName.Contains(searchConsultantLastName)).ToList();
            }
            if (searchConsultantPersonalid.HasValue)
            {
                salesInfoByProductPrices = salesInfoByProductPrices.Where(s => s.ConsultantPersonalId == searchConsultantPersonalid).ToList();
            }
            if (searchProductAmount.HasValue)
            {
                salesInfoByProductPrices = salesInfoByProductPrices.Where(s => s.ProductAmount == searchProductAmount).ToList();
            }
            if (searchSaleId.HasValue)
            {
                salesInfoByProductPrices = salesInfoByProductPrices.Where(s => s.SaleId == searchSaleId).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.ConsultantIdDesc:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderByDescending(s => s.ConsultantId).ToList();
                    break;
                case FilteringNames.LastNameDesc:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderByDescending(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.NameDesc:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderByDescending(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.SaleDateDesc:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderByDescending(s => s.SalesDate).ToList();
                    break;
                case FilteringNames.PersonalIdDesc:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderByDescending(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.AmountDesc:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderByDescending(s => s.ProductAmount).ToList();
                    break;
                case FilteringNames.SaleIdDesc:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderByDescending(s => s.SaleId).ToList();
                    break;
                case FilteringNames.LastName:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderBy(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.Name:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderBy(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.SaleDate:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderBy(s => s.SalesDate).ToList();
                    break;
                case FilteringNames.PersonalId:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderBy(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.Amount:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderBy(s => s.ProductAmount).ToList();
                    break;
                case FilteringNames.SaleId:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderBy(s => s.SaleId).ToList();
                    break;
                default:
                    salesInfoByProductPrices = salesInfoByProductPrices.OrderBy(s => s.ConsultantId).ToList();
                    break;
            }
            var pagedData = salesInfoByProductPrices.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(salesInfoByProductPrices.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        public ActionResult GetSaleInfoByConsultant(string sortOrder, int? searchConsultantId, string searchConsultantName, string searchConsultantLastName, long? searchConsultantPersonalid, long? searchSaleId, int? searchProductAmount, int? searchTotalPrice, DateTime? startDate, DateTime? endDate, int? page)
        {
            if (!startDate.HasValue || !endDate.HasValue)
            {
                return View(new List<SaleInfoByConsultant>().ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize));
            }
            ModifySort(sortOrder);
            ModifySearchingParameter(startDate, endDate);
            ViewBag.CurrentSort = sortOrder;

            ModifyConsultantFiltering(searchConsultantId, searchConsultantName, searchConsultantLastName, searchConsultantPersonalid);
            ViewBag.CurrentFilterSaleId = searchSaleId;
            ViewBag.CurrentFilterProductAmount = searchProductAmount;
            ViewBag.CurrentFilterTotalPrice = searchTotalPrice;

            var saleInfos = _consultantDal.GetSaleInfoByConsultants(startDate.Value, endDate.Value);
            if (searchConsultantId.HasValue)
            {
                saleInfos = saleInfos.Where(s => s.ConsultantId == searchConsultantId).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantName))
            {
                saleInfos = saleInfos.Where(s => s.ConsultantName.Contains(searchConsultantName)).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantLastName))
            {
                saleInfos = saleInfos.Where(s => s.ConsultantLastName.Contains(searchConsultantLastName)).ToList();
            }
            if (searchConsultantPersonalid.HasValue)
            {
                saleInfos = saleInfos.Where(s => s.ConsultantPersonalId == searchConsultantPersonalid).ToList();
            }
            if (searchSaleId.HasValue)
            {
                saleInfos = saleInfos.Where(s => s.SaleId == searchSaleId).ToList();
            }
            if (searchProductAmount.HasValue)
            {
                saleInfos = saleInfos.Where(s => s.ProductAmount == searchProductAmount).ToList();
            }
            if (searchTotalPrice.HasValue)
            {
                saleInfos = saleInfos.Where(s => s.TotalPrice == searchTotalPrice).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.ConsultantIdDesc:
                    saleInfos = saleInfos.OrderByDescending(s => s.ConsultantId).ToList();
                    break;
                case FilteringNames.SaleIdDesc:
                    saleInfos = saleInfos.OrderByDescending(s => s.SaleId).ToList();
                    break;
                case FilteringNames.TotalPriceDesc:
                    saleInfos = saleInfos.OrderByDescending(s => s.TotalPrice).ToList();
                    break;
                case FilteringNames.LastNameDesc:
                    saleInfos = saleInfos.OrderByDescending(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.NameDesc:
                    saleInfos = saleInfos.OrderByDescending(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.SaleDateDesc:
                    saleInfos = saleInfos.OrderByDescending(s => s.SalesDate).ToList();
                    break;
                case FilteringNames.PersonalIdDesc:
                    saleInfos = saleInfos.OrderByDescending(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.AmountDesc:
                    saleInfos = saleInfos.OrderByDescending(s => s.ProductAmount).ToList();
                    break;
                case FilteringNames.LastName:
                    saleInfos = saleInfos.OrderBy(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.Name:
                    saleInfos = saleInfos.OrderBy(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.SaleDate:
                    saleInfos = saleInfos.OrderBy(s => s.SalesDate).ToList();
                    break;
                case FilteringNames.PersonalId:
                    saleInfos = saleInfos.OrderBy(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.Amount:
                    saleInfos = saleInfos.OrderBy(s => s.ProductAmount).ToList();
                    break;
                case FilteringNames.SaleId:
                    saleInfos = saleInfos.OrderBy(s => s.SaleId).ToList();
                    break;
                case FilteringNames.TotalPrice:
                    saleInfos = saleInfos.OrderBy(s => s.TotalPrice).ToList();
                    break;
                default:
                    saleInfos = saleInfos.OrderBy(s => s.ConsultantId).ToList();
                    break;
            }
            var pagedData = saleInfos.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(saleInfos.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        public ActionResult GetFrequentlySaledProducts(string sortOrder, int? searchConsultantId, string searchConsultantName, string searchConsultantLastName, long? searchConsultantPersonalid, DateTime? searchConsultantBirthDate, int? searchAmount, int? saledProductsMinAmount, long? productId, DateTime? startDate, DateTime? endDate, int? page)
        {
            if (!saledProductsMinAmount.HasValue || !startDate.HasValue || !endDate.HasValue)
            {
                return View(new List<FrequentlySaledProduct>().ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize));
            }
            ModifySort(sortOrder);
            ModifySearchingParameter(startDate, endDate, saledProductsMinAmount, productId);
            ViewBag.CurrentSort = sortOrder;

            ModifyConsultantFiltering(searchConsultantId, searchConsultantName, searchConsultantLastName, searchConsultantPersonalid, searchConsultantBirthDate);
            ViewBag.CurrentFilterAmount = searchAmount;
            
            var frequentlySaledProducts = _consultantDal.GetFrequentlySaledProducts(saledProductsMinAmount.Value, productId, startDate.Value, endDate.Value);
            if (searchConsultantId.HasValue)
            {
                frequentlySaledProducts = frequentlySaledProducts.Where(s => s.ConsultantId == searchConsultantId).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantName))
            {
                frequentlySaledProducts = frequentlySaledProducts.Where(s => s.ConsultantName.Contains(searchConsultantName)).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantLastName))
            {
                frequentlySaledProducts = frequentlySaledProducts.Where(s => s.ConsultantLastName.Contains(searchConsultantLastName)).ToList();
            }
            if (searchConsultantPersonalid.HasValue)
            {
                frequentlySaledProducts = frequentlySaledProducts.Where(s => s.ConsultantPersonalId == searchConsultantPersonalid).ToList();
            }
            if (searchConsultantBirthDate != null)
            {
                frequentlySaledProducts = frequentlySaledProducts.Where(s => s.ConsultantBirthDate.Day == searchConsultantBirthDate.Value.Day
                                                     && s.ConsultantBirthDate.Month == searchConsultantBirthDate.Value.Month
                                                     && s.ConsultantBirthDate.Year == searchConsultantBirthDate.Value.Year).ToList();
            }
            if (searchAmount.HasValue)
            {
                frequentlySaledProducts = frequentlySaledProducts.Where(s => s.ProductAmount == searchAmount).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.ConsultantIdDesc:
                    frequentlySaledProducts = frequentlySaledProducts.OrderByDescending(s => s.ConsultantId).ToList();
                    break;
                case FilteringNames.LastNameDesc:
                    frequentlySaledProducts = frequentlySaledProducts.OrderByDescending(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.NameDesc:
                    frequentlySaledProducts = frequentlySaledProducts.OrderByDescending(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.BirthDayDesc:
                    frequentlySaledProducts = frequentlySaledProducts.OrderByDescending(s => s.ConsultantBirthDate).ToList();
                    break;
                case FilteringNames.PersonalIdDesc:
                    frequentlySaledProducts = frequentlySaledProducts.OrderByDescending(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.AmountDesc:
                    frequentlySaledProducts = frequentlySaledProducts.OrderByDescending(s => s.ProductAmount).ToList();
                    break;
                case FilteringNames.ProductIdDesc:
                    frequentlySaledProducts = frequentlySaledProducts.OrderByDescending(s => s.ProductId).ToList();
                    break;
                case FilteringNames.LastName:
                    frequentlySaledProducts = frequentlySaledProducts.OrderBy(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.Name:
                    frequentlySaledProducts = frequentlySaledProducts.OrderBy(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.BirthDay:
                    frequentlySaledProducts = frequentlySaledProducts.OrderBy(s => s.ConsultantBirthDate).ToList();
                    break;
                case FilteringNames.PersonalId:
                    frequentlySaledProducts = frequentlySaledProducts.OrderBy(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.Amount:
                    frequentlySaledProducts = frequentlySaledProducts.OrderBy(s => s.ProductAmount).ToList();
                    break;
                case FilteringNames.ProductId:
                    frequentlySaledProducts = frequentlySaledProducts.OrderBy(s => s.ProductId).ToList();
                    break;
                default:
                    frequentlySaledProducts = frequentlySaledProducts.OrderBy(s => s.ConsultantId).ToList();
                    break;
            }
            var pagedData = frequentlySaledProducts.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(frequentlySaledProducts.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        public ActionResult GetProfitableProducts(string sortOrder, int? searchConsultantId, string searchConsultantName, string searchConsultantLastName, long? searchConsultantPersonalid, DateTime? searchConsultantBirthDate, string searchMaxSaledProductName, string searchMaxProfitProductName, int? searchMaxSaledProductAmount, decimal? searchMaxProfitSum, long? searchMaxSaledProductId, long? searchMaxProfitProductId, DateTime? startDate, DateTime? endDate, int? page)
        {
            ModifySort(sortOrder);
            ModifySearchingParameter(startDate, endDate);
            ViewBag.CurrentSort = sortOrder;

            ModifyConsultantFiltering(searchConsultantId, searchConsultantName, searchConsultantLastName, searchConsultantPersonalid, searchConsultantBirthDate);
            ViewBag.CurrentFilterMaxSaledProductId = searchMaxSaledProductId;
            ViewBag.CurrentFilterMaxSaledProductName = searchMaxSaledProductName;
            ViewBag.CurrentFilterMaxSaledProductAmount = searchMaxSaledProductAmount;
            ViewBag.CurrentFilterMaxProfitProductId = searchMaxProfitProductId;
            ViewBag.CurrentFilterMaxProfitProductName = searchMaxProfitProductName;
            ViewBag.CurrentFilterMaxProfitSum = searchMaxProfitSum;

            var profitableProducts = _consultantDal.GetProfitableProducts(startDate, endDate);
            if (searchConsultantId.HasValue)
            {
                profitableProducts = profitableProducts.Where(s => s.ConsultantId == searchConsultantId).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantName))
            {
                profitableProducts = profitableProducts.Where(s => s.ConsultantName.Contains(searchConsultantName)).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantLastName))
            {
                profitableProducts = profitableProducts.Where(s => s.ConsultantLastName.Contains(searchConsultantLastName)).ToList();
            }
            if (searchConsultantPersonalid.HasValue)
            {
                profitableProducts = profitableProducts.Where(s => s.ConsultantPersonalId == searchConsultantPersonalid).ToList();
            }
            if (searchConsultantBirthDate != null)
            {
                profitableProducts = profitableProducts.Where(s => s.ConsultantBirthDate.Day == searchConsultantBirthDate.Value.Day
                                                     && s.ConsultantBirthDate.Month == searchConsultantBirthDate.Value.Month
                                                     && s.ConsultantBirthDate.Year == searchConsultantBirthDate.Value.Year).ToList();
            }
            if (searchMaxSaledProductId.HasValue)
            {
                profitableProducts = profitableProducts.Where(s => s.MaxSaledProductId == searchMaxSaledProductId).ToList();
            }
            if (searchMaxProfitProductId.HasValue)
            {
                profitableProducts = profitableProducts.Where(s => s.MaxSaledProductId == searchMaxProfitProductId).ToList();
            }
            if (!string.IsNullOrEmpty(searchMaxSaledProductName))
            {
                profitableProducts = profitableProducts.Where(s => s.MaxSaledProductName.Contains(searchMaxSaledProductName)).ToList();
            }
            if (!string.IsNullOrEmpty(searchMaxProfitProductName))
            {
                profitableProducts = profitableProducts.Where(s => s.MaxProfitProductName.Contains(searchMaxProfitProductName)).ToList();
            }
            if (searchMaxSaledProductAmount.HasValue)
            {
                profitableProducts = profitableProducts.Where(s => s.MaxSaledProductAmount == searchMaxSaledProductAmount).ToList();
            }
            if (searchMaxProfitSum.HasValue)
            {
                profitableProducts = profitableProducts.Where(s => s.MaxProfitTotalSum == searchMaxProfitSum).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.ConsultantIdDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.ConsultantId).ToList();
                    break;
                case FilteringNames.LastNameDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.NameDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.BirthDayDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.ConsultantBirthDate).ToList();
                    break;
                case FilteringNames.PersonalIdDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.MaxSaledProductIdDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.MaxSaledProductId).ToList();
                    break;
                case FilteringNames.MaxSaledProductNameDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.MaxSaledProductName).ToList();
                    break;
                case FilteringNames.MaxSaledProductAmountDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.MaxSaledProductAmount).ToList();
                    break;
                case FilteringNames.MaxProfitProductIdDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.MaxProfitProductId).ToList();
                    break;
                case FilteringNames.MaxProfitProductNameDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.MaxProfitProductName).ToList();
                    break;
                case FilteringNames.MaxProfitSumDesc:
                    profitableProducts = profitableProducts.OrderByDescending(s => s.MaxProfitTotalSum).ToList();
                    break;
                case FilteringNames.LastName:
                    profitableProducts = profitableProducts.OrderBy(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.Name:
                    profitableProducts = profitableProducts.OrderBy(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.BirthDay:
                    profitableProducts = profitableProducts.OrderBy(s => s.ConsultantBirthDate).ToList();
                    break;
                case FilteringNames.PersonalId:
                    profitableProducts = profitableProducts.OrderBy(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.MaxSaledProductId:
                    profitableProducts = profitableProducts.OrderBy(s => s.MaxSaledProductId).ToList();
                    break;
                case FilteringNames.MaxSaledProductName:
                    profitableProducts = profitableProducts.OrderBy(s => s.MaxSaledProductName).ToList();
                    break;
                case FilteringNames.MaxSaledProductAmount:
                    profitableProducts = profitableProducts.OrderBy(s => s.MaxSaledProductAmount).ToList();
                    break;
                case FilteringNames.MaxProfitProductId:
                    profitableProducts = profitableProducts.OrderBy(s => s.MaxProfitProductId).ToList();
                    break;
                case FilteringNames.MaxProfitProductName:
                    profitableProducts = profitableProducts.OrderBy(s => s.MaxProfitProductName).ToList();
                    break;
                case FilteringNames.MaxProfitSum:
                    profitableProducts = profitableProducts.OrderBy(s => s.MaxProfitTotalSum).ToList();
                    break;
                default:
                    profitableProducts = profitableProducts.OrderBy(s => s.ConsultantId).ToList();
                    break;
            }
            var pagedData = profitableProducts.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(profitableProducts.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        public ActionResult GetConsultantsSalesCount(string sortOrder, int? searchConsultantId, string searchConsultantName, string searchConsultantLastName, long? searchConsultantPersonalid, DateTime? searchConsultantBirthDate, int? searchAmount, int? searchHierarchyAmount, DateTime? startDate, DateTime? endDate, int? page)
        {
            ModifySort(sortOrder);
            ModifySearchingParameter(startDate, endDate);
            ViewBag.CurrentSort = sortOrder;

            ModifyConsultantFiltering(searchConsultantId, searchConsultantName, searchConsultantLastName, searchConsultantPersonalid, searchConsultantBirthDate);
            ViewBag.CurrentFilterAmount = searchAmount;
            ViewBag.CurrentFilterHierarchyAmount = searchHierarchyAmount;

            var salesCount = _consultantDal.GetConsultantsSalesCount(startDate, endDate);
            if (searchConsultantId.HasValue)
            {
                salesCount = salesCount.Where(s => s.ConsultantId == searchConsultantId).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantName))
            {
                salesCount = salesCount.Where(s => s.ConsultantName.Contains(searchConsultantName)).ToList();
            }
            if (!string.IsNullOrEmpty(searchConsultantLastName))
            {
                salesCount = salesCount.Where(s => s.ConsultantLastName.Contains(searchConsultantLastName)).ToList();
            }
            if (searchConsultantPersonalid.HasValue)
            {
                salesCount = salesCount.Where(s => s.ConsultantPersonalId == searchConsultantPersonalid).ToList();
            }
            if (searchConsultantBirthDate != null)
            {
                salesCount = salesCount.Where(s => s.ConsultantBirthDate.Day == searchConsultantBirthDate.Value.Day
                                                     && s.ConsultantBirthDate.Month == searchConsultantBirthDate.Value.Month
                                                     && s.ConsultantBirthDate.Year == searchConsultantBirthDate.Value.Year).ToList();
            }
            if (searchAmount.HasValue)
            {
                salesCount = salesCount.Where(s => s.SalesAmount == searchAmount).ToList();
            }
            if (searchHierarchyAmount.HasValue)
            {
                salesCount = salesCount.Where(s => s.HierarchySalesAmount == searchHierarchyAmount).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.ConsultantIdDesc:
                    salesCount = salesCount.OrderByDescending(s => s.ConsultantId).ToList();
                    break;
                case FilteringNames.LastNameDesc:
                    salesCount = salesCount.OrderByDescending(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.NameDesc:
                    salesCount = salesCount.OrderByDescending(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.BirthDayDesc:
                    salesCount = salesCount.OrderByDescending(s => s.ConsultantBirthDate).ToList();
                    break;
                case FilteringNames.PersonalIdDesc:
                    salesCount = salesCount.OrderByDescending(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.AmountDesc:
                    salesCount = salesCount.OrderByDescending(s => s.SalesAmount).ToList();
                    break;
                case FilteringNames.HierarchyAmountDesc:
                    salesCount = salesCount.OrderByDescending(s => s.HierarchySalesAmount).ToList();
                    break;
                case FilteringNames.LastName:
                    salesCount = salesCount.OrderBy(s => s.ConsultantLastName).ToList();
                    break;
                case FilteringNames.Name:
                    salesCount = salesCount.OrderBy(s => s.ConsultantName).ToList();
                    break;
                case FilteringNames.BirthDay:
                    salesCount = salesCount.OrderBy(s => s.ConsultantBirthDate).ToList();
                    break;
                case FilteringNames.PersonalId:
                    salesCount = salesCount.OrderBy(s => s.ConsultantPersonalId).ToList();
                    break;
                case FilteringNames.Amount:
                    salesCount = salesCount.OrderBy(s => s.SalesAmount).ToList();
                    break;
                case FilteringNames.HierarchyAmount:
                    salesCount = salesCount.OrderBy(s => s.HierarchySalesAmount).ToList();
                    break;
                default:
                    salesCount = salesCount.OrderBy(s => s.ConsultantId).ToList();
                    break;
            }
            var pagedData = salesCount.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(salesCount.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        private void ModifySort(string sortOrder)
        {
            ViewBag.ConsultantIdSortParam = string.IsNullOrEmpty(sortOrder) ? FilteringNames.ConsultantIdDesc : "";
            ViewBag.ConsultantNameSortParam = sortOrder == FilteringNames.Name ? FilteringNames.NameDesc : FilteringNames.Name;
            ViewBag.ConsultantLastNameSortParam = sortOrder == FilteringNames.LastName ? FilteringNames.LastNameDesc : FilteringNames.LastName;
            ViewBag.ConsultantPersonalIdSortParam = sortOrder == FilteringNames.PersonalId ? FilteringNames.PersonalIdDesc : FilteringNames.PersonalId;
            ViewBag.BirthDateSortParam = sortOrder == FilteringNames.BirthDay ? FilteringNames.BirthDayDesc : FilteringNames.BirthDay;
            ViewBag.HierarchySalesAmountSortParam = sortOrder == FilteringNames.HierarchyAmount ? FilteringNames.HierarchyAmountDesc : FilteringNames.HierarchyAmount;
            ViewBag.MaxSaledProductIdSortParam = sortOrder == FilteringNames.MaxSaledProductId ? FilteringNames.MaxSaledProductIdDesc : FilteringNames.MaxSaledProductId;
            ViewBag.MaxSaledProductNameSortParam = sortOrder == FilteringNames.MaxSaledProductName ? FilteringNames.MaxSaledProductNameDesc : FilteringNames.MaxSaledProductName;
            ViewBag.MaxSaledProductAmountSortParam = sortOrder == FilteringNames.MaxSaledProductAmount ? FilteringNames.MaxSaledProductAmountDesc : FilteringNames.MaxSaledProductAmount;
            ViewBag.MaxProfitProductIdSortParam = sortOrder == FilteringNames.MaxProfitProductId ? FilteringNames.MaxProfitProductIdDesc : FilteringNames.MaxProfitProductId;
            ViewBag.MaxProfitProductNameSortParam = sortOrder == FilteringNames.MaxProfitProductName ? FilteringNames.MaxProfitProductNameDesc : FilteringNames.MaxProfitProductName;
            ViewBag.MaxProfitSumSortParam = sortOrder == FilteringNames.MaxProfitSum ? FilteringNames.MaxProfitSumDesc : FilteringNames.MaxProfitSum;
            ViewBag.ProductIdSortParam = sortOrder == FilteringNames.ProductId ? FilteringNames.ProductIdDesc : FilteringNames.ProductId;
            ViewBag.SaleDateSortParam = sortOrder == FilteringNames.SaleDate ? FilteringNames.SaleDateDesc : FilteringNames.SaleDate;
            ViewBag.AmountSortParam = sortOrder == FilteringNames.Amount ? FilteringNames.AmountDesc : FilteringNames.Amount;
            ViewBag.SaleIdSortParam = sortOrder == FilteringNames.SaleId ? FilteringNames.SaleIdDesc : FilteringNames.SaleId;
            ViewBag.TotalPriceSortParam = sortOrder == FilteringNames.TotalPrice ? FilteringNames.TotalPriceDesc : FilteringNames.TotalPrice;
        }

        private void ModifySearchingParameter(DateTime? startDate = null, DateTime? endDate = null, int? saledProductsMinAmount = null,
            long? productId = null, decimal? minPrice = null, decimal? maxPrice = null)
        {
            ViewBag.SearchParameterStartDate = startDate;
            ViewBag.SearchParameterEndDate = endDate;
            ViewBag.SearchParameterSaledProductMinAmount = saledProductsMinAmount;
            ViewBag.SearchParameterProductId = productId;
            ViewBag.SearchParameterMinPrice = minPrice;
            ViewBag.SearchParameterMaxPrice = maxPrice;
        }

        private void ModifyConsultantFiltering(int? searchConsultantId, string searchConsultantName, string searchConsultantLastName,
            long? searchConsultantPersonalid, DateTime? searchConsultantBirthDate = null)
        {
            ViewBag.CurrentFilterConsultantId = searchConsultantId;
            ViewBag.CurrentFilterConsultantName = searchConsultantName;
            ViewBag.CurrentFilterConsultantLastName = searchConsultantLastName;
            ViewBag.CurrentFilterConsultantPersonalId = searchConsultantPersonalid;
            ViewBag.CurrentFilterConsultantBirthDate = searchConsultantBirthDate;
        }
    }
}
