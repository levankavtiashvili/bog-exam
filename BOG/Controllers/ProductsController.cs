﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using BOG.Constants.Filtering;
using BOG.Constants.Paging;
using BOG.DAL.Implementation;
using BOG.Helper;
using BOG.Models;
using PagedList;

namespace BOG.Controllers
{
    public class ProductsController : Controller
    {
        private readonly Bog _db = new Bog();

        // GET: Products
        public ActionResult Index(string sortOrder, int? searchId, string searchName, decimal? searchPrice, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilterId = searchId;
            ViewBag.CurrentFilterName = searchName;
            ViewBag.CurrentFilterPrice = searchPrice;
            ViewBag.IdSortParam = string.IsNullOrEmpty(sortOrder) ? FilteringNames.IdDesc : "";
            ViewBag.NameSortParam = sortOrder == FilteringNames.Name ? FilteringNames.NameDesc : FilteringNames.Name;
            ViewBag.PriceSortParam = sortOrder == FilteringNames.Price ? FilteringNames.PriceDesc : FilteringNames.Price;
            var products = _db.Products.ToList();
            if (searchId.HasValue)
            {
                products = products.Where(s => s.Id == searchId).ToList();
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                products = products.Where(s => s.Name.Contains(searchName)).ToList();
            }
            if (searchPrice.HasValue)
            {
                products = products.Where(s => s.Price == searchPrice).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.IdDesc:
                    products = products.OrderByDescending(s => s.Id).ToList();
                    break;
                case FilteringNames.PriceDesc:
                    products = products.OrderByDescending(s => s.Price).ToList();
                    break;
                case FilteringNames.NameDesc:
                    products = products.OrderByDescending(s => s.Name).ToList();
                    break;
                case FilteringNames.Name:
                    products = products.OrderBy(s => s.Name).ToList();
                    break;
                case FilteringNames.Price:
                    products = products.OrderBy(s => s.Price).ToList();
                    break;
                default:
                    products = products.OrderBy(s => s.Id).ToList();
                    break;
            }
            var pagedData = products.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(products.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        // GET: Products/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = await _db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Price")] Product product)
        {
            if (product.Price <= 0)
            {
                throw new Exception("Price Is Equal Or Less Than Zero");
            }
            if (!ModelState.IsValid)
            {
                return View(product);
            }
            _db.Products.Add(product);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        // GET: Products/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = await _db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Price")] Product product)
        {
            if (product.Price <= 0)
            {
                throw new Exception("Price Is Equal Or Less Than Zero");
            }
            if (!ModelState.IsValid)
            {
                return View(product);
            }
            _db.Entry(product).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Products/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = await _db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            _db.Products.Remove(await _db.Products.FindAsync(id) ?? throw new InvalidOperationException());
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
