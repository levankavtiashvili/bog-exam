﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using BOG.Constants.Filtering;
using BOG.Constants.Paging;
using BOG.DAL.Implementation;
using BOG.Helper;
using BOG.Models;
using PagedList;

namespace BOG.Controllers
{
    public class SalesController : Controller
    {
        private readonly Bog _db = new Bog();

        // GET: Sales
        public ActionResult Index(string sortOrder, int? searchId, string searchName, string searchLastName, int? searchConsultantId, DateTime? searchCreatedAt, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilterId = searchId;
            ViewBag.CurrentFilterName = searchName;
            ViewBag.CurrentFilterLastName = searchLastName;
            ViewBag.CurrentFilterConsultantId = searchConsultantId;
            ViewBag.CurrentFilterCreatedAt = searchCreatedAt;
            ViewBag.IdSortParam = string.IsNullOrEmpty(sortOrder) ? FilteringNames.IdDesc : "";
            ViewBag.NameSortParam = sortOrder == FilteringNames.Name ? FilteringNames.NameDesc : FilteringNames.Name;
            ViewBag.LastNameSortParam = sortOrder == FilteringNames.LastName ? FilteringNames.LastNameDesc : FilteringNames.LastName;
            ViewBag.CreatedAtSortParam = sortOrder == FilteringNames.CreatedAt ? FilteringNames.CreatedAtDesc : FilteringNames.CreatedAt;
            ViewBag.ConsultantIdSortParam = sortOrder == FilteringNames.ConsultantId ? FilteringNames.ConsultantIdDesc : FilteringNames.ConsultantId;
            var sales = _db.Sales.ToList();
            if (searchId.HasValue)
            {
                sales = sales.Where(s => s.Id == searchId).ToList();
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                sales = sales.Where(s => s.Consultant.Name.Contains(searchName)).ToList();
            }
            if (!string.IsNullOrEmpty(searchLastName))
            {
                sales = sales.Where(s => s.Consultant.LastName.Contains(searchLastName)).ToList();
            }
            if (searchConsultantId.HasValue)
            {
                sales = sales.Where(s => s.ConsultantId == searchConsultantId.Value).ToList();
            }
            if (searchCreatedAt != null)
            {
                sales = sales.Where(s => s.CreatedAt.Day == searchCreatedAt.Value.Day
                                                     && s.CreatedAt.Month == searchCreatedAt.Value.Month
                                                     && s.CreatedAt.Year == searchCreatedAt.Value.Year).ToList();
            }
            switch (sortOrder)
            {
                case FilteringNames.IdDesc:
                    sales = sales.OrderByDescending(s => s.Id).ToList();
                    break;
                case FilteringNames.LastNameDesc:
                    sales = sales.OrderByDescending(s => s.Consultant.LastName).ToList();
                    break;
                case FilteringNames.NameDesc:
                    sales = sales.OrderByDescending(s => s.Consultant.Name).ToList();
                    break;
                case FilteringNames.ConsultantIdDesc:
                    sales = sales.OrderByDescending(s => s.ConsultantId).ToList();
                    break;
                case FilteringNames.CreatedAtDesc:
                    sales = sales.OrderByDescending(s => s.CreatedAt).ToList();
                    break;
                case FilteringNames.Name:
                    sales = sales.OrderBy(s => s.Consultant.Name).ToList();
                    break;
                case FilteringNames.LastName:
                    sales = sales.OrderBy(s => s.Consultant.LastName).ToList();
                    break;
                case FilteringNames.CreatedAt:
                    sales = sales.OrderBy(s => s.CreatedAt).ToList();
                    break;
                case FilteringNames.ConsultantId:
                    sales = sales.OrderBy(s => s.ConsultantId).ToList();
                    break;
                default:
                    sales = sales.OrderBy(s => s.Id).ToList();
                    break;
            }
            var pagedData = sales.ToPagedList(PageFormatter.GetValidPage(page), Paging.PageSize);
            if (!pagedData.Any() && PageFormatter.GetValidPage(page) != 1)
            {
                return View(sales.ToPagedList(1, Paging.PageSize));
            }
            return View(pagedData);
        }

        // GET: Sales/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var sale = await _db.Sales.FindAsync(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            sale.SaleDetails = _db.SaleDetails.Where(x => x.SaleId == id.Value).ToList();
            return View(sale);
        }

        // GET: Sales/Create
        public ActionResult Create()
        {
            ViewBag.ConsultantIds = _db.Consultants.ToListAsync().Result;
            return View();
        }

        // POST: Sales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,ConsultantId,CreatedAt")] Sale sale)
        {
            if (!ModelState.IsValid)
            {
                return View(sale);
            }
            sale.CreatedAt = DateTime.Now;
            _db.Sales.Add(sale);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Sales/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var sale = await _db.Sales.FindAsync(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            return View(sale);
        }

        // POST: Sales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,ConsultantId,CreatedAt")] Sale sale)
        {
            if (!ModelState.IsValid)
            {
                return View(sale);
            }
            _db.Entry(sale).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Sales/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var sale = await _db.Sales.FindAsync(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            return View(sale);
        }

        // POST: Sales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            _db.Sales.Remove(await _db.Sales.FindAsync(id) ?? throw new InvalidOperationException());
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
