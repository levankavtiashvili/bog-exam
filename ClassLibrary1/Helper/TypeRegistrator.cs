﻿using BOG.Models.DAL.Implementation;
using BOG.Models.DAL.Interface;
using ClassLibrary1.DAL.Implementation;
using StructureMap;

namespace BOG.Models.Helper
{
    public class TypeRegistrator : Registry
    {
        public TypeRegistrator()
        {
            Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
            
            For<IConsultantDal>().Use<ConsultantDal>();
        }
    }
}