﻿using System;
using System.Collections.Generic;
using BOG.Models.SearchModels;

namespace BOG.Models.DAL.Interface
{
    public interface IConsultantDal
    {
        List<SaleInfoByProductPrice> GetSaleInfoProductPrice(decimal minPrice, decimal maxPrice, DateTime startDate, DateTime endDate);

        List<SaleInfoByConsultant> GetSaleInfoByConsultants(DateTime startDate, DateTime endDate);

        List<FrequentlySaledProduct> GetFrequentlySaledProducts(int saledProductsMinAmount, long? productId, DateTime startDate, DateTime endDate);
    }
}