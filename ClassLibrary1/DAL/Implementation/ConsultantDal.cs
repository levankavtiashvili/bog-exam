﻿using System;
using System.Collections.Generic;
using BOG.Models.DAL.Implementation;
using BOG.Models.DAL.Interface;
using BOG.Models.SearchModels;

namespace ClassLibrary1.DAL.Implementation
{
    public class ConsultantDal : BaseDal, IConsultantDal
    {
        public List<SaleInfoByProductPrice> GetSaleInfoProductPrice(decimal minPrice, decimal maxPrice, DateTime startDate, DateTime endDate)
        {
            var saleInfo = new List<SaleInfoByProductPrice>();
            using (var ccw = GetSpCommand("spGetSaleInfoByProductPrice"))
            {
                var cmd = ccw.SqlCommand;
                AddParameter(cmd, "MinPrice", minPrice);
                AddParameter(cmd, "MaxPrice", maxPrice);
                AddParameter(cmd, "StartDate", startDate);
                AddParameter(cmd, "EndDate", endDate);
                using (var dr = cmd.ExecuteReader())
                {
                    while(dr.Read())
                    {
                        saleInfo.Add(new SaleInfoByProductPrice
                        {
                            SaleId = Convert.ToInt64(dr["SaleId"]),
                            ConsultantId = Convert.ToInt32(dr["ConsultantId"]),
                            ConsultantLastName = Convert.ToString(dr["LastName"]),
                            ConsultantName = Convert.ToString(dr["Name"]),
                            ConsultantPersonalId = Convert.ToInt64(dr["PersonalId"]),
                            ProductAmount = Convert.ToInt32(dr["ProductAmount"]),
                            SalesDate = Convert.ToDateTime(dr["SalesDate"])
                        });
                    }
                }
            }
            return saleInfo;
        }

        public List<SaleInfoByConsultant> GetSaleInfoByConsultants(DateTime startDate, DateTime endDate)
        {
            var saleInfo = new List<SaleInfoByConsultant>();
            using (var ccw = GetSpCommand("spGetSaleInfoByConsultants"))
            {
                var cmd = ccw.SqlCommand;
                AddParameter(cmd, "StartDate", startDate);
                AddParameter(cmd, "EndDate", endDate);
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        saleInfo.Add(new SaleInfoByConsultant
                        {
                            SaleId = Convert.ToInt64(dr["SaleId"]),
                            ConsultantId = Convert.ToInt32(dr["ConsultantId"]),
                            ConsultantLastName = Convert.ToString(dr["LastName"]),
                            ConsultantName = Convert.ToString(dr["Name"]),
                            ConsultantPersonalId = Convert.ToInt64(dr["PersonalId"]),
                            ProductAmount = Convert.ToInt32(dr["ProductAmount"]),
                            TotalPrice = Convert.ToDecimal(dr["TotalPrice"]),
                            SalesDate = Convert.ToDateTime(dr["SalesDate"])
                        });
                    }
                }
            }
            return saleInfo;
        }

        public List<FrequentlySaledProduct> GetFrequentlySaledProducts(int saledProductsMinAmount, long? productId, DateTime startDate, DateTime endDate)
        {
            var saleInfo = new List<FrequentlySaledProduct>();
            using (var ccw = GetSpCommand("spGetFrequentlySaledProducts"))
            {
                var cmd = ccw.SqlCommand;
                AddParameter(cmd, "SaledProductsMinAmount", saledProductsMinAmount);
                AddParameter(cmd, "ProductId", productId);
                AddParameter(cmd, "StartDate", startDate);
                AddParameter(cmd, "EndDate", endDate);
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        saleInfo.Add(new FrequentlySaledProduct
                        {
                            ConsultantId = Convert.ToInt32(dr["ConsultantId"]),
                            ConsultantLastName = Convert.ToString(dr["LastName"]),
                            ConsultantName = Convert.ToString(dr["Name"]),
                            ConsultantPersonalId = Convert.ToInt64(dr["PersonalId"]),
                            ProductAmount = Convert.ToInt32(dr["ProductAmount"]),
                            BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                            ProductId = Convert.ToInt64(dr["ProductId"])
                        });
                    }
                }
            }
            return saleInfo;
        }

        public List<MostlySaledProduct> GetFrequentlySaledProducts(DateTime? startDate, DateTime? endDate)
        {
            var saleInfo = new List<MostlySaledProduct>();
            using (var ccw = GetSpCommand("spGetProfitableProducts"))
            {
                var cmd = ccw.SqlCommand;
                AddParameter(cmd, "StartDate", startDate);
                AddParameter(cmd, "EndDate", endDate);
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        saleInfo.Add(new MostlySaledProduct
                        {
                            ConsultantId = Convert.ToInt32(dr["ConsultantId"]),
                            ConsultantLastName = Convert.ToString(dr["LastName"]),
                            ConsultantName = Convert.ToString(dr["Name"]),
                            ConsultantPersonalId = Convert.ToInt64(dr["PersonalId"]),
                            ProductAmount = Convert.ToInt32(dr["ProductAmount"]),
                            BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                            ProductId = Convert.ToInt64(dr["ProductId"])
                        });
                    }
                }
            }
            return saleInfo;
        }
    }
}